<?php

namespace Drupal\media_embed_focal_point\Plugin\Filter;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\media\MediaInterface;
use Drupal\media\Plugin\Filter\MediaEmbed;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to embed media items using a custom tag.
 *
 * @Filter(
 *   id = "media_embed_focal_point",
 *   title = @Translation("Embed media with Focal Point"),
 *   description = @Translation("Focal point support for embedded Media. Place before the 'Embed Media' filter."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   settings = {
 *     "format" = "",
 *     "default_view_mode" = "default",
 *     "allowed_view_modes" = {}
 *   },
 *   weight = 99,
 * )
 *
 * @internal
 */
class MediaEmbedFocalPoint extends MediaEmbed implements ContainerFactoryPluginInterface, TrustedCallbackInterface {

  /**
   * The current route service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected CurrentRouteMatch $currentRoute;

  /**
   * Constructs a MediaEmbedFocalPoint object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRoute
   *   The current route service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityRepositoryInterface $entity_repository, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository, EntityTypeBundleInfoInterface $bundle_info, RendererInterface $renderer, LoggerChannelFactoryInterface $logger_factory, CurrentRouteMatch $currentRoute) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_repository, $entity_type_manager, $entity_display_repository, $bundle_info, $renderer, $logger_factory);
    $this->currentRoute = $currentRoute;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.repository'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('renderer'),
      $container->get('logger.factory'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): MediaEmbedFocalPoint {
    parent::setConfiguration($configuration);

    // Process with the settings from media embed filter.
    if (array_key_exists('format', $this->settings)) {
      $format_config =  \Drupal::config('filter.format.' . $this->settings['format']);
      $media_embed_settings = $format_config->get('filters.media_embed.settings');
      if ($media_embed_settings) {
        $this->settings['default_view_mode'] = $media_embed_settings['default_view_mode'];
        $this->settings['allowed_view_modes'] = $media_embed_settings['allowed_view_modes'];
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    // Add a hidden field to store the current filter format.
    $filter_format = $this->currentRoute->getParameter('filter_format');
    if ($filter_format) {
      $form['format'] = [
        '#type' => 'hidden',
        '#value' => $filter_format->id()
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function applyPerEmbedMediaOverrides(\DOMElement $node, MediaInterface $media) {
    parent::applyPerEmbedMediaOverrides($node, $media);
    if ($image_field = $this->getMediaImageSourceField($media)) {
      if ($node->hasAttribute('data-focal-point')) {
        $media->{$image_field}->media_embed_focal_point = $node->getAttribute('data-focal-point');
        $node->removeAttribute('data-focal-point');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    if (isset($this->settings['default_view_mode']) && isset($this->settings['allowed_view_modes'])) {
      parent::calculateDependencies();
    }
  }

}
