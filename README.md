# Media Embed Focal Point
Integrates the focal point widget into media embed forms.

## Requirements

* [Focal Point] configured for use with image media.
* On the text format, the "Embed media" filter must be enabled.
* On the text format, the Media Library button must be added to the editor.


## Usage
1. Install module.
2. Enable the "Embed media with Focal Point" filter on your text format, and set it to process before the "Embed Media" filter.
3. Add `data-focal-point` to your text formats "Allowed HTML tags", within the `<drupal-media>` tag.

## Technical details

This modules builds off of the great experimental work being done on the [Media Library Media Modify] and [Multi Crop] modules. As such, this modules is also experimental.

[Focal Point]: https://www.drupal.org/project/focal_point
[Media Library Media Modify]: https://www.drupal.org/project/media_library_media_modify
[Multi Crop]: https://www.drupal.org/project/multi_crop
