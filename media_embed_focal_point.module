<?php

use Drupal\Component\Utility\Html;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\media\MediaInterface;

/**
 * Implements hook_form_alter().
 *
 * @param &$form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 * @param string $form_id
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 * @noinspection PhpUnused
 */
function media_embed_focal_point_form_alter(&$form, FormStateInterface $form_state, string $form_id) {
  _media_embed_focal_point_widget_add($form, $form_state, $form_id);
}

/**
 * Integrate the focal_point form widget.
 *
 * @param &$form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 * @param $form_id
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function _media_embed_focal_point_widget_add(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id === 'editor_media_dialog') {
    $media_embed_element = $form_state->get('media_embed_element');
    /** @var \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository */
    $entityRepository = \Drupal::service('entity.repository');
    /** @var MediaInterface $media */
    $media = $entityRepository->loadEntityByUuid('media', $media_embed_element['data-entity-uuid']);
    $source_field_name = _media_embed_focal_point_get_media_image_source_field_name($media);
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository */
    $entity_display_repository = \Drupal::service('entity_display.repository');
    $media_library_form_display = $entity_display_repository->getFormDisplay('media', $media->bundle(), 'media_library');
    $component = $media_library_form_display->getComponent($source_field_name);
    if ($component && $component['type'] === 'image_focal_point') {
      $widget = $media_library_form_display->getRenderer($source_field_name);
      $items = $media->get($source_field_name);
      $items->filterEmptyItems();
      $form[$source_field_name] = $widget->form($items,$form, $form_state);
      $form[$source_field_name]['#parents'] = [];
      $form[$source_field_name]['#weight'] = -10;
      if (array_key_exists('data-focal-point', $media_embed_element)) {
        $form[$source_field_name]['widget'][0]['#default_value']['focal_point'] = $media_embed_element['data-focal-point'];
      }
      $form[$source_field_name]['widget'][0]['#alt_field'] = false;
      $form[$source_field_name]['widget'][0]['#alt_field_required'] = false;
      $form[$source_field_name]['widget'][0]['#process'][] = '_media_embed_focal_point_widget_modify';
      $form['actions']['save_modal']['#submit'][] = '_media_embed_focal_point_widget_save';
    }
  }
}

/**
 * Save the form value as a data attribute on the media embed element.
 *
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function _media_embed_focal_point_widget_save($form, FormStateInterface $form_state) {
  $focal_point = $form_state->getValue(['field_media_image', 0, 'focal_point']);
  if ($focal_point) {
    $form_state->setValue(['attributes', 'data-focal-point'], $focal_point);
  }
}

/**
 * Hides upload, remove button, and file link from file widget. Handles modal.
 *
 * @param array $element
 *   The form element to process.
 *
 * @return array
 *   The processed form element.
 * @noinspection PhpUnused
 */
function _media_embed_focal_point_widget_modify(array $element): array {
  $remove_access = [
    'upload_button',
    'remove_button'
  ];

  // Remove the file link
  foreach ($element['#files'] as $file) {
    $remove_access[] = 'file_' . $file->id();
  }

  foreach ($remove_access as $key) {
    $element[$key]['#access'] = FALSE;
  }

  // Since core does not support nested modal dialogs, we need to ensure that
  // the preview page opens in a new tab, rather than a modal dialog via AJAX.
  $preview_link_attributes = &$element['preview']['preview_link']['#attributes'];
  unset($preview_link_attributes['data-dialog-type']);
  $preview_link_attributes['class'] = array_diff($preview_link_attributes['class'], ['use-ajax']);
  return $element;
}

/**
 * Gets the name of an image media item's source field.
 *
 * @param \Drupal\media\MediaInterface $media
 *   The media item being embedded.
 *
 * @return string|null
 *   The name of the image source field configured for the media item, or
 *   NULL if the source field is not an image field.
 */
function _media_embed_focal_point_get_media_image_source_field_name(MediaInterface $media): ?string {
  $field_definition = $media->getSource()
    ->getSourceFieldDefinition($media->bundle->entity);
  $item_class = $field_definition->getItemDefinition()->getClass();
  if (is_a($item_class, ImageItem::class, TRUE)) {
    return $field_definition->getName();
  }
  return NULL;
}

/**
 * Modeled after the way the multi_crop module works, this creates a new crop
 * using the crop anchor set within the editor edit dialog.
 *
 * @param &$variables
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @noinspection PhpUnused
 */
function media_embed_focal_point_preprocess_image_formatter(&$variables) {
  _media_embed_focal_point_apply_crop($variables);
}

/**
 * During the filter processing, the focal point is retrieved from wysiwyg.
 * If set, it is attached to the image object.
 *
 * @param &$variables
 *
 * @return void
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function _media_embed_focal_point_apply_crop(&$variables) {
  if ($focal_point = $variables['item']->media_embed_focal_point) {

    [$anchor_x, $anchor_y] = explode(',', $focal_point);

    // Model the attributes after the multi_crop focal_point plugin.
    $variables['media_embed_focal_point_attributes'] = [
//      'plugin_id' => 'focal_point', // currently unused
//      'crop_setting' => $focal_point, // currently unused
      'context' => sprintf('x%s:y%s', $anchor_x, $anchor_y),
      'image_style' => $variables['image_style']
    ];

    // The code below accomplishes the same thing, independently of:
    //  - media_library_media_modify
    //  - multi_crop
    //
    //  The hope is that media_library_media_modify adds support for media embeds
    //  and that multi_crop fully works, with support for media embeds as well.

    // Transform the uri of the image to a new context specific location.
    // @see multi_crop/multi_crop.module::template_preprocess_multi_crop_image_formatter()
    $old_uri = $variables['image']['#uri'];
    $new_uri = _media_embed_focal_point_transform_image_uri($variables['image']['#uri'], $variables['media_embed_focal_point_attributes']);
    $variables['image']['#uri'] = $new_uri;

    // Get Crop type.
    // @see \Drupal\multi_crop\Plugin\MultiCrop\FocalPoint.php::saveCrop
    $crop_type = \Drupal::config('focal_point.settings')->get('crop_type');

    // Retrieve Crop.
    // @see \Drupal\multi_crop\MultiCropPluginBase::retrievCrop
    $crop = _media_embed_focal_point_retrieve_crop($crop_type, $old_uri, $new_uri);

    // Modify the crop with the embedded focal point data.
    // @see \Drupal\multi_crop\Plugin\MultiCrop\FocalPoint.php::saveCrop

    // Recalculate coordinate.
    $absolute = \Drupal::service('focal_point.manager')->relativeToAbsolute($anchor_x, $anchor_y, $variables['image']['#width'], $variables['image']['#height']);

    // Set anchor.
    $anchor = $crop->anchor();
    if ($anchor['x'] != $absolute['x'] || $anchor['y'] != $absolute['y']) {
      $crop->setPosition($absolute['x'], $absolute['y']);
      $crop->save();
    }
  }
}

/**
 * Tries to retrieve an existing crop for this context, or creates a new one.
 *
 * @param string $crop_type
 * @param string $old_uri
 * @param string $new_uri
 *
 * @return false|mixed|null
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 *
 * @see \Drupal\multi_crop\MultiCropPluginBase::retrievCrop
 */
function _media_embed_focal_point_retrieve_crop(string $crop_type, string $old_uri, string $new_uri) {
  $cropStorage = \Drupal::service('entity_type.manager')->getStorage('crop');

  // Try to load crop for the current context.
  $crop = $cropStorage->loadByProperties(['uri' => $new_uri]);
  $crop = reset($crop) ?: NULL;

  // Try to load crop from source image.
  if ($crop == NULL) {

    /** @var \Drupal\file\FileInterface[] $files */
    $files = \Drupal::entityTypeManager()
      ->getStorage('file')
      ->loadByProperties(['uri' => $old_uri]);
    /** @var \Drupal\file\FileInterface|null $file */
    $file = reset($files) ?: NULL;

    $values = [
      'type' => $crop_type,
      'entity_id' => $file->id(),
      'entity_type' => 'file',
      'uri' => $new_uri,
    ];

    // Create new cron.
    $crop = $cropStorage->create($values);
  }

  return $crop;
}

/**
 * Copy source image to dedicated place.
 *
 * @param string $image_uri
 *   Source image URI
 * @param array $crop_data
 *   Crop elements.
 *
 * @return string
 *   New image source.
 *
 * @see _multi_crop_transform_image_uri()
 */
function _media_embed_focal_point_transform_image_uri(string $image_uri, array $crop_data): string {

  $context = Html::cleanCssIdentifier($crop_data['context']);

  // Transform public://path/image to public://style/media_embed_focal_point/[context]/path/image.
  $uri_data = explode('//', $image_uri);
  $image_path = array_pop($uri_data);
  $path = explode('/', $image_path);
  $image_name = array_pop($path);
  $file_path = implode('/', $path);
  $new_path = $uri_data[0]. '//styles/media_embed_focal_point/' . $context . '/'. $file_path;
  $new_uri = $new_path . '/' . $image_name;

  // Use file_system services to create copy in targeted path.
  $filesystem = \Drupal::service('file_system');
  $filesystem->prepareDirectory($new_path, FileSystemInterface::CREATE_DIRECTORY);
  $filesystem->prepareDirectory($new_path, FileSystemInterface::MODIFY_PERMISSIONS);
  $filesystem->copy($image_uri, $new_uri, FileSystemInterface::EXISTS_REPLACE);

  // calculate derivative URL, remove file if exist
  $crop_type = $crop_data['image_style'];

  $derivative = $uri_data[0]. '//styles/'.  $crop_type .'/public/styles/media_embed_focal_point/' . $context . '/'. $file_path . '/' . $image_name;
  if(file_exists($filesystem->realpath($derivative))) {
    unlink($filesystem->realpath($derivative));
  }
  return $new_uri;
}
